Scripts to quickly pull basic stats from GitLab JSON logs.  These are in the process of being rewritten in Ruby

**Note**: For Shell scripts [jq](https://stedolan.github.io/jq/) must be in your $PATH

### Adding a new report
We warmly encourage you to add any queries you have found useful, here's what you need to know to add to a Ruby script:

   1. Find the appropriate file
      * prod_stats.rb => production_json.log
      * api_stats.rb => api_json.log
      * gitaly_stats.rb => current
   1. Add a new method to the class defined in the file
      * Parsed JSON/gitaly is in the `@data` variable
      * Method must return an array of hashes
      * Hash keys will be used for column headers
      * All hashes are expected to have the same keys
      * The `@count` variable contains the number of items requested by the user, if relevant
      * Add the method name as a key in the `REPORTS` hash, with the title of the report as its value
   
### Scripts for production_json.log
#### prod_stats.rb
Defaults to printing 25 results per report in plain-text format

**Flags:**

   * `-c / --count <COUNT>` -- Number of results to return
   * `-f / --format <FORMAT>` -- Print results as text, md, or csv
   * `-r / --report <REPORT>` -- Print a specific report

**Available reports:**

  * slowest_calls -- Slowest Calls
  * top_paths -- Top Paths by Volume
  * top_err_paths -- Top Paths by 5XX Errors
  * top_users -- Top Active Users
  * top_ips -- Top IP Addresses
  * controller_stats -- Controller Stats

|CONTROLLER|COUNT|PERC99|PERC95|STDDEV|MAX|MIN|SCORE|
|--|--|--|--|--|--|--|--|
|JwtController#auth|6438|206.51|183.2|38.53|425.7|6.81|1329511.4|
|Projects::MergeRequestsController#show|347|1451.83|863.49|311.89|3304.39|6.61|503785.0|
|Projects::PipelinesController#index|78|4800.66|4496.7|1625.57|4869.71|7.7|374451.5|
|RootController#index|19703|11.62|4.91|64.98|3464.61|2.98|228948.9|
|Projects::JobsController#show|170|1114.25|861.36|269.76|1586.48|8.15|189422.5|
|Projects::RefsController#logs_tree|63|2964.52|2370.32|660.94|3189.66|302.82|186764.8|
     
`SCORE` is PERC99 * COUNT

---

### Scripts for api_json.log
#### api_stats.rb
Defaults to printing 25 results per report in plain-text format

**Flags:**

   * `-c / --count <COUNT>` -- Number of results to return
   * `-f / --format <FORMAT>` -- Print results as text, md, or csv
   * `-r / --report <REPORT>` -- Print a specific report

**Available reports:**

  * slowest_calls -- Slowest Calls
  * top_paths -- Top Paths by Volume
  * top_err_paths -- Top Paths by 5XX Errors
  * top_routes -- Top Routes by Volume
  * top_err_routes -- Top Routes by 5XX Errors
  * top_users -- Top Active Users
  * top_ips -- Top IP Addresses
  * route_stats -- Route Stats

|ROUTE|COUNT|PERC99|PERC95|STDDEV|MAX|MIN|SCORE|
|--|--|--|--|--|--|--|--|
|/api/:version/projects/:id/repository/files/:file_path/raw|12762|615.78|485.57|855.34|33102.01|150.28|7858584.4|
|/api/:version/projects/:id/repository/branches|1158|1815.95|967.19|1009.28|24568.61|413.13|2102870.1|
|/api/:version/jobs/:id/trace|5764|192.72|48.26|93.92|3073.61|25.97|1110838.1|
|/api/:version/internal/allowed|10031|46.11|30.17|193.86|8497.34|12.48|462529.4|
|/api/:version/projects|575|756.49|646.46|67.96|1340.81|5.24|434981.8|

---

### Scripts for production_json.log and api_json.log
#### intervals.sh
Outputs the top 5 paths accessed in 5 minute chunks.

```
2019-02-20T19:52:08.517Z
COUNT           PERC95          PERC99          PATH
227             115.73          130.86          /group/project.git/info/refs
196             125.90          135.87          /group/project.git/git-upload-pack
155             39.68           49.93           /uploads/-/system/project/avatar/1/user.jpeg
139             25.79           35.05           /uploads/-/system/appearance/header_logo/1/logo.jpg
95              94.12           131.84          /group/project/subproject/merge_request/1/notes

2019-02-20T19:57:08.566Z
COUNT           PERC95          PERC99          PATH
225             131.51          150.64          /group/project.git/git-upload-pack
147             122.79          139.11          /group/project.git/info/refs
79              108.40          130.77          /group/project/merge_request/1/notes
67              60.94           77.70           /group/subgroup/project/merge_request/1/notes
51              31.95           54.61           /uploads/-/system/appearance/header_logo/1/logo.jpg
```  

---

### Scripts for Gitaly logs
**Note**: [pritaly]https://gitlab.com/gitlab-com/support/toolbox/pritaly) must be in your $PATH

#### gitaly_stats.rb
Volume and performance stats of for each Gitaly method.

**Flags:**

   * `-c / --count <COUNT>` -- Number of results to return
   * `-f / --format <FORMAT>` -- Print results as text, md, or csv


|METHOD|COUNT|PERC99|PERC95|STDDEV|MAX|MIN|SCORE|
|--|--|--|--|--|--|--|--|
|SSHUploadPack|18277|4918.4|935.28|751.75|17680.46|65.04|89893596.8|
|InfoRefsUploadPack|1672|18991.97|4493.11|2965.05|27343.9|62.45|31754573.8|
|CreateBundle|1539|16357.96|5054.66|3978.1|90393.67|217.22|25174900.4|
|FindCommit|56091|135.15|111.92|210.92|11391.59|29.07|7580698.7|
|TreeEntry|25667|162.26|143.82|273.21|12208.41|40.77|4164727.4|
|PostUploadPack|332|9243.08|8586.27|2761.18|9899.71|90.35|3068702.6|

#### gitaly_method_intervals.sh
Outputs the top 5 methods accessed in 5 minute chunks, these parameters can be
changed with the `-c <COUNT>` and `-m <MINUTES>` options.

```
2019-03-06T17:25:35Z
Total Calls: 1686

COUNT           MEDIAN          PERC99          PERC95          METHOD
652             113             10.91           9.13            FindCommit
248             241             153.61          251.71          SSHUploadPack
108             1239            8.86            11.70           ListCommitsByOid
99              29              0.86            0.84            IsRebaseInProgress
92              92              3.72            3.62            RefExists

2019-03-06T17:30:35Z
Total Calls: 471

COUNT           MEDIAN          PERC99          PERC95          METHOD
169             241             4075.86         4517.49         SSHUploadPack
163             113             5616.61         7138.45         FindCommit
38              1239            4413.89         4750.01         ListCommitsByOid
25              29              1.45            1.38            IsRebaseInProgress
13              8               5122.42         5122.42         TreeEntry
```

#### gitaly_project_intervals.sh
Outputs the top 5 projects accessed in 5 minute chunks

```
2019-02-20_20:22:08.64248
COUNT           PROJECT
4148            group/project.git
432             group/subgroup/project.git
218             other_group/project.git
200             another_group/project.git
197             another_group/project1.git

2019-02-20_20:27:08.77067
COUNT           PROJECT
2898            group/project.git
389             group/subgroup/project.git
330             another_group/project1.git
206             other_group/project.git
172             other_group/project2.git
```
     
---

### Misc Scripts
#### corr_id.sh
Search for events related to a correlation_id in all logs. Results are sorted by time.

By default it will search recursively in the current directory, but another path can be specified using the `-p <PATH>` option.

```bash
$ corr_id.sh -p ../ 0f5EC7LOBV
 ../gitlab-rails/production_json.log:{"method":"GET","path":"/root/test/merge_requests/1.json","format":"json",
     "controller":"Projects::MergeRequestsController","action":"show","status":200,"duration":212.49,"view":1.07,
     "db":27.62,"time":"2019-02-25T01:10:00.772Z","params":[{"key":"serializer","value":"widget"},
     {"key":"namespace_id","value":"root"},{"key":"project_id","value":"test"},{"key":"id","value":"1"}],
     "remote_ip":"1.1.1.1","user_id":1,"username":"root","ua":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0)
     Gecko/20100101 Firefox/59.0","gitaly_calls":6,"correlation_id":"0f5EC7LOBV"}
 ../gitaly/current:2019-02-25_01:10:00.80727 time="2019-02-25T01:10:00Z" level=info msg="finished unary call with code OK" 
     correlation_id=0f5EC7LOBV grpc.code=OK grpc.meta.auth_version=v2 grpc.meta.client_name=gitlab-web grpc.method=IsRebaseInProgress 
     grpc.request.deadline="2019-02-25T01:10:10Z" grpc.request.fullMethod=/gitaly.RepositoryService/IsRebaseInProgress 
     grpc.request.glRepository=project-1 grpc.request.repoPath=root/test.git grpc.request.repoStorage=default 
     grpc.request.topLevelGroup=root grpc.service=gitaly.RepositoryService grpc.start_time="2019-02-25T01:10:00Z" 
     grpc.time_ms=0.088 peer.address=@ span.kind=server system=grpc
 ```