require 'json'
require 'optparse'

DEFAULT_COUNT = 25
DEFAULT_FORMAT = :text

class Stats
  def self.execute(file_name, reports, default_report)
    options = StatsOptionParser.parse(reports.keys)
    report = options[:report].nil? ? default_report : options[:report]

    if ARGV.empty? then 
      stats = self.new(nil, options[:count], options[:format], reports)
    else
      stats = self.new(ARGV[0], options[:count], options[:format], reports)
    end

    stats.print_report(report)
  end

  def initialize(file_name, ct, format, reports)
      @count = ct.nil? ? DEFAULT_COUNT : ct.to_i
      @formatter = StatsFormatter.new(format, reports)
      @available_reports = reports.keys

    begin
      if file_name.nil?
        input = ARGF.read.split("\n")
      else
        input = File.read(file_name).split("\n")
      end

      @data = self.parse_input(input)
      @start_time = parse_time(input.first)
      @end_time = parse_time(input.last)
    rescue Errno::ENOENT => err
      abort(err.to_s)
    end
  end

  def parse_input(data)
    begin
      data.map{|line| JSON.parse(line, :symbolize_names => true)}
    rescue JSON::ParserError => err
      abort("Invalid JSON:\n" + err.to_s)
    end
  end

  def print_report(report)
    puts "Start time: #{@start_time}"
    puts "End time: #{@end_time}"
    puts

    if report == :all
      @available_reports.each do |r|
        @formatter.print_results(r, self.send(r))
        puts
      end
    else
      @formatter.print_results(report, self.send(report))
    end
  end

  def percentile(p, nums)
    nums.sort!

    return 0 if nums.empty?
    return nums.first if nums.length == 1
    return nums.last if p == 100

    idx = p / 100.0 * (nums.length - 1)
    lower, upper = nums[idx.floor,2]
    lower + (upper - lower) * (idx - idx.floor)
  end

  def stddev(nums)
    return 0 if nums.length == 1

    mean = nums.inject(:+) / nums.length.to_f
    var_sum = nums.map{|n| (n - mean) ** 2}.inject(:+).to_f
    sample_variance = var_sum / (nums.length - 1)
    Math.sqrt(sample_variance)
  end

  def durations(objs)
    objs.map{|entry| entry[:duration]}.sort!
  end

  def parse_time(line)
    event = JSON.parse(line, :symbolize_names => true)
    event[:time]
  end
end

class StatsFormatter
  def initialize(format_type, reports)
      @format = format_type.nil? ? DEFAULT_FORMAT : format_type
      @reports = reports
  end

  def print_results(report, results)
    case @format
    when :text
      print_text(report, results)
    when :md
      print_markdown(report, results)
    when :csv
      print_csv(report, results)
    end
  end

  def print_text(report, results)
    return if results.empty?

    widths = max_widths(results)
    headers = results
      .first
      .map do |k, v|
        if v.is_a? Integer or v.is_a? Float
          "#{k.to_s.upcase.rjust(widths[k])}"
        else
          "#{k.to_s.upcase.ljust(widths[k])}"
        end
    end.join("    ")

    puts @reports[report]
    puts headers

    results.each do |r|
      r.each do |k, v|
        if v.is_a? Integer or v.is_a? Float
          print "#{v.to_s.rjust(widths[k])}    "
        else
          print "#{v.to_s.ljust(widths[k])}    "
        end
      end
      puts
    end
  end

  def max_widths(results)
    widths = {}

    results.first.each_key do |key|
      key_width = key.to_s.length
      widest_val = results.max_by{|val| val[key].to_s.length}[key].to_s.length

      widths[key] = widest_val > key_width ? widest_val : key_width
    end

    widths
  end

  def print_markdown(report, results)
    return if results.empty?

    headers = results
      .first
      .each_key
      .map{|key| key.to_s.upcase}
      .join("|")

    puts "#### #{@reports[report]}"
    puts "|" + headers + "|"
    puts "|" + "--|" * results.first.length

    results.each do |line|
      delimited_line = line
        .each_value
        .map{|val| val.to_s}
        .join("|")

      puts "|" + delimited_line + "|"
    end
  end

  def print_csv(report, results)
    return if results.empty?

    puts @reports[report]

    puts results
      .first.each_key
      .map{|key| key.to_s.upcase}
      .join(",")

    results.each do |line|
      puts line
        .each_value
        .map{|val| val.to_s}
        .join(",")
    end
  end
end

class StatsOptionParser
  def self.parse(reports)
    options = {}
    all_reports = [:all] + reports

    opt_parse = OptionParser.new do |opts|
    opts.banner = "Usage: #{$PROGRAM_NAME} [options] <FILE>"

    count_desc = "Number of results to return (default " + DEFAULT_COUNT.to_s + ")"
    opts.on("-cCOUNT", "--count COUNT", Integer, count_desc) do |c|
      options[:count] = c
    end

    opts.on("-fFORMAT", "--format FORMAT", [:text, :md, :csv],
           "Format to print results (default text) [text, md, csv]") do |f|
      options[:format] = f
    end

    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      exit
    end

    reports_desc = "Format to print results (default all) [" + all_reports.map{|r| r.to_s}.join(", ") + "]"
    opts.on("-rREPORT", "--report REPORT", all_reports, reports_desc) do |r|
      options[:report] = r
    end
  end


  begin
    opt_parse.parse!
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption => err
    puts(err.to_s)
    abort(opt_parse.help)
  end

  options
  end
end
