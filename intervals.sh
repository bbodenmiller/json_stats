#!/bin/bash

usage() { echo "Useage: $0 [-c <COUNT>] [-m <MINUTES>] <FILE>"; exit; }

while getopts "c:m:" opt; do
  case $opt in
    c)
      COUNT=$OPTARG
      ;;
    m)
      WINDOW=$OPTARG
      ;;
    \?)
      usage
      ;;
  esac
done
shift $(($OPTIND-1))

FILE=$1

if [ -z $COUNT ]; then
  COUNT=5
fi

if [ -z $WINDOW ]; then
  WINDOW=5
fi

(( WINDOW_SECS = $WINDOW * 60 ))

# Validate that a file was passed
if [ -z "$FILE" ]
then
  echo "Usage: $0 <FILE>"
  exit
fi

# Verify that jq is available
jq --version > /dev/null
rc=$?

if [ $rc != 0 ]
then
  echo "jq not found"
  exit
fi

top_by_chunks=`jq -s 'def gettime: explode | (.[20:23] | implode | tonumber / 1000) as $frac | .[0:19] | implode | . + "Z" | fromdate | . + $frac; \
  def starttime: .[0].time | gettime; \
  def add_egroup($s): ((.time | (gettime - $s) / '$WINDOW_SECS') | floor) as $egrp | . + {egroup: $egrp}; \
  def add_egroup_time($et): . + {etime: $et}; \
  def perc($i): ($i * length | floor) as $idx | .[$idx].duration; \
  def top_paths: group_by(.path) \
  | sort_by(-length) \
  | [ limit('$COUNT'; .[]) \
  | sort_by(.duration) \
  | {batch: .[0].etime, count: length, path: .[0].path, perc99: perc(0.99), perc95: perc(0.95)} ];
  starttime as $st \
  | map(add_egroup($st)) \
  | group_by(.egroup) \
  | .[] \
  | .[0].time as $etime \
  | map(add_egroup_time($etime)) \
  | length as $total_len \
  | top_paths \
  | .[] \
  | "\(.batch) \(.count) \(.perc99) \(.perc95) \(.path) \($total_len)"' $FILE \
  | tr -d '"' \
  | awk 'BEGIN { print "Top '$COUNT' Paths per '$WINDOW' Minute Span" }; \
  { if ($1 != prev)
    { printf "\n%s\nTotal calls: %s\n\n%-8s\t%-10s\t%-10s\t%s\n%-8i\t%-8.2f\t%-8.2f\t%s\n", $1, $6, "COUNT", "PERC99", "PERC95", "PATH", $2, $3, $4, $5; }
  else
    { printf "%-8i\t%-8.2f\t%-8.2f\t%s\n", $2, $3, $4, $5; }
  prev = $1 }; \
  END { printf "\r\n" }'`

echo "$top_by_chunks"
