#!/bin/bash

usage() { echo "Useage: $0 [-c <COUNT>] [-m <MINUTES>] <FILE>"; exit; }

while getopts "c:m:" opt; do
  case $opt in
    c)
      COUNT=$OPTARG
      ;;
    m)
      WINDOW=$OPTARG
      ;;
    \?)
      usage
      ;;
  esac
done
shift $(($OPTIND-1))

FILE=$1

if [ -z $COUNT ]; then
  COUNT=5
fi

if [ -z $WINDOW ]; then
  WINDOW=5
fi

(( WINDOW_SECS = $WINDOW * 60 ))

if [ -z $FILE ]; then
  usage
fi

# Validate that a file was passed
if [ -z "$FILE" ]
then
  echo "Usage: $0 <FILE>"
  exit
fi

# Verify that jq is available
jq --version > /dev/null
rc=$?

if [ $rc != 0 ]
then
  echo "jq not found"
  exit
fi

# Verify that pritaly is available
pritaly --version > /dev/null
rc=$?

if [ $rc != 0 ]
then
  echo "pritaly not found"
  exit
fi

top_by_chunks=`pritaly -j $FILE \
  | jq -s 'def gettime: sub("_"; "T") | explode | (.[20:25] | implode | tonumber / 100000) as $frac | .[0:19] | implode | . + "Z" | fromdate | . + $frac; \
  def starttime: .[0].log_time | gettime; \
  def add_egroup($s): ((.log_time | (gettime - $s) / '$WINDOW_SECS') | floor) as $egrp | . + {egroup: $egrp} + {etime: ($s + $egrp * '$WINDOW_SECS' | todate)}; \
  def max_egroup: unique_by(.egroup) | [ .[].egroup ] | max; \
  def zerofill_lengths($maxlen): [ .[] | length ] | until (length >= $maxlen; . + [0]); \
  def add_median($maxlen): group_by(.grpc.method) | .[] | group_by(.egroup) | (zerofill_lengths($maxlen) | sort_by(-length) | (length / 2 | floor)) as $med_idx | ( sort_by(length) | .[$med_idx] | length) as $med_len | .[][] | . + {median_len: $med_len}; \
  def perc($i): ($i * length | floor) as $idx | .[$idx].grpc.time_ms; \
  def top_methods: group_by(.grpc.method) \
  | sort_by(-length) \
  | [ .[] \
  | sort_by(.grpc.time_ms) \
  | {batch: .[0].etime, count: length, method: .[0].grpc.method, median_len: .[0].median_len, perc99: perc(0.99), perc95: perc(0.95)} ]; \
  starttime as $st \
  | [ map(select(.grpc.method != null)) \
  | map(add_egroup($st)) \
  | max_egroup as $max_grp \
  | add_median($max_grp) ] \
  | group_by(.egroup) \
  | .[] \
  | length as $total_calls \
  | top_methods \
  | limit('$COUNT'; .[]) \
  | "\(.batch) \(.count) \(.median_len) \(.perc99) \(.perc95) \(.method) \($total_calls)"' \
  | tr -d '"' \
  | awk 'BEGIN { print "Top '$COUNT' Methods per '$WINDOW' Minute Span" }; \
  { if ($1 != prev)
    { printf "\n%s\nTotal Calls: %s\n\n%-8s\t%-8s\t%-10s\t%-10s\t%s\n%-8i\t%-8i\t%-8.2f\t%-8.2f\t%s\n", $1, $7, "COUNT", "MEDIAN_CT", "PERC99", "PERC95", "METHOD", $2, $3, $4, $5, $6; }
  else
    { printf "%-8i\t%-8i\t%-8.2f\t%-8.2f\t%s\n", $2, $3, $4, $5, $6; }
  prev = $1 }; \
  END { printf "\r\n" }'`

echo "$top_by_chunks"
  #| group_by(.grpc.method) | .[] \
  #| {method: .[0].grpc.method, len: length, durs: [ .[].grpc.time_ms ] | sort }'` \
